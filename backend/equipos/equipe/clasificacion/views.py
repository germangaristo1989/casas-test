
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from clasificacion.models import Clasificacion
from clasificacion.serializers import ClasificacionSerializer,ClasificacionCreateSerializer
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.permissions import AllowAny
# Create your views here.



class ClasificacionViewSet(generics.ListAPIView ):
    queryset = Clasificacion.objects.all()
    serializer_class = ClasificacionSerializer
    permission_classes = [AllowAny]
 
    @swagger_auto_schema(operation_id="getAllClasificacion", operation_description="Obtener lista de Clasificacion",    tags=['clasificacion'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ClasificacionCreateView(generics.CreateAPIView):
    queryset = Clasificacion.objects.all()
    serializer_class = ClasificacionCreateSerializer
    permission_classes = [AllowAny]
 
    @swagger_auto_schema(operation_id="createClasificacion", operation_description="Crear nueva Clasificacion" , tags=['clasificacion'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetClasificacionView(APIView):  
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="getClasificacion",
        responses={200: ClasificacionSerializer(many=False)},
        tags=['clasificacion'],
    )
    def get(self, request, pk):
        """
        Return list of Clasificacion
        """
        try:
            user = Clasificacion.objects.get(id=pk)
            return Response(ClasificacionSerializer(user).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar esa Clasificacion'}, status=HTTP_404_NOT_FOUND)

class DeleteClasificacionView(APIView):      
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="deleteclasificacion",
        serializer_class = ClasificacionCreateSerializer,
        tags=['clasificacion']
    )
    def delete(self, request, pk):
        """
        Delete torneo
        """
        current_torneo = Clasificacion.objects.get(id=pk)
        current_torneo.delete()      
        return Response({'status': 'delete successfull!'})
