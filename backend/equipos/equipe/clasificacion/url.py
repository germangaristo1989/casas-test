from django.urls import include, path

from clasificacion.views import (ClasificacionViewSet as clasificacionView,
GetClasificacionView as clasificacionGetView,
ClasificacionCreateView as clasificacionCreateView,
DeleteClasificacionView as clasificacionDeleteView
)

__url_clasificacion = [
    path('', clasificacionView.as_view()),
    path('/<int:pk>', clasificacionGetView.as_view()),
    path('/create', clasificacionCreateView.as_view()),
    path('/<int:pk>/delete', clasificacionDeleteView.as_view())
 
]

urlpatterns = [
    path('clasificacion', include(__url_clasificacion)),
]
