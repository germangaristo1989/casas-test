from django.urls import include, path

from torneo.views import (TorneoViewSet as torneoView,
GetTorneoView as torneoGetView,
TorneoCreateView as torneoCreateView,
DeleteTorneoView as torneoDeleteView
                         )

__url_torneo = [
    path('', torneoView.as_view()),
    path('/<int:pk>', torneoGetView.as_view()),
    path('/create', torneoCreateView.as_view()),
    path('/<int:pk>/delete', torneoDeleteView.as_view())
 
]

urlpatterns = [
    path('torneo', include(__url_torneo)),
]
