
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from torneo.models import Torneo
from torneo.serializers import TorneoSerializer,TorneoCreateSerializer
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.permissions import AllowAny
# Create your views here.



class TorneoViewSet(generics.ListAPIView ):
    queryset = Torneo.objects.all()
    serializer_class = TorneoSerializer
    permission_classes = [AllowAny]
 
    @swagger_auto_schema(operation_id="getAllTorneo", operation_description="Obtener lista de Torneos",    tags=['torneo'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class TorneoCreateView(generics.CreateAPIView):
    queryset = Torneo.objects.all()
    serializer_class = TorneoCreateSerializer
    permission_classes = [AllowAny]
 
    @swagger_auto_schema(operation_id="createTorneo", operation_description="Crear nueva Torneo" , tags=['torneo'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetTorneoView(APIView):  
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="getTorneo",
        responses={200: TorneoSerializer(many=False)},
        tags=['torneo'],
    )
    def get(self, request, pk):
        """
        Return list of torneo
        """
        try:
            user = Torneo.objects.get(id=pk)
            return Response(TorneoSerializer(user).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese torneo'}, status=HTTP_404_NOT_FOUND)

class DeleteTorneoView(APIView):      
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="deletetorneo",
        serializer_class = TorneoCreateSerializer,
        tags=['torneo']
    )
    def delete(self, request, pk):
        """
        Delete torneo
        """
        current_torneo = Torneo.objects.get(id=pk)
        current_torneo.delete()      
        return Response({'status': 'delete successfull!'})
