from rest_framework.relations import PrimaryKeyRelatedField
from cargo.models import Cargo
from cargo.serializer import CargoSerializer
from directiva.models import Directiva
from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import  ModelSerializer



class DirectivaCreateSerializer(ModelSerializer):
    
    id = ReadOnlyField()
    cargo=PrimaryKeyRelatedField(queryset = Cargo.objects.all())
    class Meta:
        ref_name = 'DirectiveCreate'
        model= Directiva
        fields=['id','nombre','cargo']
  
class DirectivaSerializer(ModelSerializer):
    cargo= CargoSerializer(many=False, read_only=True)
    class Meta:
        ref_name = 'Directive'
        model= Directiva
        fields=['id','nombre','cargo']