from django.db import models
from jugador.models import Jugador
from entrenador.models import Entrenador
from logros.models import Logros
from directiva.models import Directiva
# Create your models here.
class Equipo(models.Model):
    nombre=models.TextField()
    jugador=models.ForeignKey(Jugador, on_delete=models.CASCADE)
    entrenador=models.ForeignKey(Entrenador, on_delete=models.CASCADE)
    logros=models.ForeignKey(Logros, on_delete=models.CASCADE)
    directiva= models.ForeignKey(Directiva,on_delete=models.CASCADE)
    

    def getTitulares(self):
        return Jugador.objects.filter(esTitular= True)

    def getSuplentes(self):
        return Jugador.objects.filter(esTitular= False)

    def cambioDeTitular(self,titular_actual,nuevo_titular):        
        actual_jugador_titular=Jugador.objects.get(id=titular_actual)
        nuevo_jugador_titular=Jugador.objects.get(id=nuevo_titular)
        actual_jugador_titular.esTitular= False
        nuevo_jugador_titular.esTitular= True
        actual_jugador_titular.save()
        nuevo_jugador_titular.save()

    def cambioTitular(self):        
        return Jugador.objects.get(esTitular= True)