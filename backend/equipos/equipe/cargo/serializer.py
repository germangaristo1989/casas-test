from cargo.models import Cargo
from rest_framework import serializers
from rest_framework.fields import CharField,ReadOnlyField
from rest_framework.serializers import  ModelSerializer, Serializer

class CargoSerializer(ModelSerializer):
    id = ReadOnlyField()
    class Meta:
        model= Cargo
        fields=['id','cargo']


class UpdateCargoSerializer(Serializer):
    """
    User update cargo
    """
    id = ReadOnlyField()
    cargo = CharField(
        max_length=None, min_length=None, allow_blank=False, trim_whitespace=True)

  