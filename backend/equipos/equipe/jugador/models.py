from django.db import models

# Create your models here.
class Jugador(models.Model):
    nombre = models.TextField()
    posicion = models.TextField()
    esMito = models.BooleanField()
    esTitular = models.BooleanField()