from rest_framework import viewsets
from rest_framework import permissions
from jugador.serializers import JugadorSerializer
from jugador.models import Jugador
from rest_framework.permissions import AllowAny
# Create your views here.

class JugadorViewSet(viewsets.ModelViewSet):
     """
     API endpoint that allows users to be viewed or edited.
     """
     queryset = Jugador.objects.all().order_by('nombre')
     serializer_class = JugadorSerializer
     permission_classes = [AllowAny]