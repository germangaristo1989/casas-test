from django.apps import AppConfig


class TipotorneoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tipoTorneo'
