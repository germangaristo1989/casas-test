from django.urls import include, path

from premios.views import (PremiosViewSet as PremiosView,
                          UpdatePremiosView as PremiosUpdateView, GetPremiosView as PremiosGetVew,DeletePremiosView as PremiosDeleteView)

__url_premios = [
    path('', PremiosView.as_view()),
    path('/<int:pk>', PremiosGetVew.as_view()),
    path('/<int:pk>/', PremiosUpdateView.as_view()),
    path('/<int:pk>/delete', PremiosDeleteView.as_view()),
 
]

urlpatterns = [
    path('premios', include(__url_premios)),
]
