# Generated by Django 3.2.7 on 2021-09-27 16:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('logros', '0003_remove_logros_idlogro'),
    ]

    operations = [
        migrations.RenameField(
            model_name='logros',
            old_name='idPremio',
            new_name='Premio',
        ),
        migrations.RenameField(
            model_name='logros',
            old_name='idTorneos',
            new_name='Torneos',
        ),
    ]
