
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from torneo.models import Torneo
from premios.models import Premios
from logros.models import Logros
from logros.serializers import LogrosSerializer,LogrosCreateSerializer
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.permissions import AllowAny
# Create your views here.



class LogrosViewSet(generics.ListAPIView ):
    queryset = Logros.objects.all()
    serializer_class = LogrosSerializer
    permission_classes = [AllowAny]
 
    @swagger_auto_schema(operation_id="getAllLogros", operation_description="Obtener lista de Logros",    tags=['logros'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class LogrosCreateView(generics.CreateAPIView):
    queryset = Logros.objects.all()
    serializer_class = LogrosCreateSerializer
    permission_classes = [AllowAny]
 
    @swagger_auto_schema(operation_id="createLogro", operation_description="Crear nuevo Logro" , tags=['logros'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetLogrosView(APIView):  
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="getLogro",
        responses={200: LogrosSerializer(many=False)},
        tags=['logros'],
    )
    def get(self, request, pk):
        """
        Return list of logros
        """
        try:
            torneo = Torneo.objects.get(id=pk)
            logros = Premios.objects.get(id=pk)
            return Response(LogrosSerializer(torneo,logros).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese Logro'}, status=HTTP_404_NOT_FOUND)

class DeleteLogrosView(APIView):      
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="deleteLogros",
        serializer_class = LogrosCreateSerializer,
        tags=['logros']
    )
    def delete(self, request, pk):
        """
        Delete logros
        """
        current_logros = Logros.objects.get(id=pk)
        current_logros.delete()      
        return Response({'status': 'delete successfull!'})
