import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { GoogleAnalyticsService } from 'ngx-google-analytics';

const ELEMENT_DATA: any[] = [
  {
    position: 1,
    name: 'Independiente',
    weight: 'Hugo Moyano',
    symbol: 'Eduardo Dominguez',
  },
  {
    position: 2,
    name: 'Boca',
    weight: 'Juan Román Riquelme',
    symbol: 'Sebastian Bataglia',
  },
  {
    position: 3,
    name: 'River',
    weight: 'Rodolfo D´onofrio',
    symbol: 'Marcelo Gallardo',
  },
  {
    position: 4,
    name: 'Racing',
    weight: 'Victor Blanco',
    symbol: 'Fernando Gago',
  },
  {
    position: 5,
    name: 'R.Madrid',
    weight: 'Florentino Perez',
    symbol: 'Carlo Ancelloti',
  },
  {
    position: 6,
    name: 'Barça',
    weight: 'Joan Laporta',
    symbol: 'Xavi Hernandez',
  },
];

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
})
export class TeamsComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  public dataSource: MatTableDataSource<any> = new MatTableDataSource();

  public filter = new FormControl('');

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private googleAnalyticsService: GoogleAnalyticsService) {
     // asi se registra el path con el nombre de la página en el evento page_view
    this.googleAnalyticsService.pageView('/teams', 'Equipo');
  }

  ngOnInit(): void {
    this.loadTeams();
  }

  ngAfterViewInit() {
    this.loadTeams();
  }

  public applyFilter() {
    this.dataSource.filter = this.filter.value.trim().toLowerCase();
  }

  public clearFilter() {
    this.filter.setValue('');
    this.dataSource.filter = '';
  }

  private loadTeams() {
    let response = [];

    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
  }
  public selectedOption(value: any) {
    //evento para enviar el equipo seleccionado como value
    this.googleAnalyticsService.event(
      'Equipo_Elegido',
      'Categoria',
      'Equipo seleccionado',
      value.name,
      false
    );
  }
}
