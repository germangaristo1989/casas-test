import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { DirectivaService } from '@casas/rest';
import { DirectiveDataSource, DirectivaItem } from './directive-datasource';
import { DialogComponent } from './dialog/dialog.component';
import { GoogleAnalyticsService } from 'ngx-google-analytics';

@Component({
  selector: 'app-directive',
  templateUrl: './directive.component.html',
  styleUrls: ['./directive.component.scss'],
})
export class DirectiveComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<DirectivaItem>;
  dataSource: any;

  displayedColumns = ['nombre', 'cargo', 'accions'];

  constructor(
    private readonly svcDirectiva: DirectivaService,
    public dialog: MatDialog,
    private googleAnalyticsService: GoogleAnalyticsService
  ) {
    // asi se registra el path con el nombre de la página en el evento page_view
    this.googleAnalyticsService.pageView('/directiva', 'Directivas');
    this.dataSource = new DirectiveDataSource();
  }

  public edit(id: any): void {
    const dialogRef = this.dialog.open(DialogComponent);

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
    });
    dialogRef.componentInstance.name = id;
    dialogRef.componentInstance.size = 'Large';
    // evento que se le envia el directiva agregada en este caso el id
    this.googleAnalyticsService.event(
      'Directiva_Elegida',
      'Categoria',
      'directiva seleccionado',
      id,
      false
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.svcDirectiva.getAllDirective().subscribe((value) => {
      this.dataSource.data = value;
      this.table.dataSource = this.dataSource;
    });
  }
}
