import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  @Input() public name: string;
  @Input() public size: string;
  constructor() {
    this.name = '';
    this.size = '';
  }

  ngOnInit(): void {
    console.log(this.name);
  }
}
