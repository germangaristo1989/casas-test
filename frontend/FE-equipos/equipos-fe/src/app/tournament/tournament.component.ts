import { AfterViewInit, Component } from '@angular/core';
import { GoogleAnalyticsService, GaActionEnum } from 'ngx-google-analytics';

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.scss'],
})
export class TournamentComponent implements AfterViewInit {
  constructor(private googleAnalyticsService: GoogleAnalyticsService) {
    // asi se registra el path con el nombre de la página en el evento page_view
    this.googleAnalyticsService.pageView('/tournaments', 'tournament');
  }

  ngAfterViewInit(): void {}
  openTournament(value: any) {
    //Evento para seleccionar, en este caso se envia al hacer click al boton
    this.googleAnalyticsService.event(
      'Torneo Seleccionado',
      'Categoria',
      'Torneo seleccionado',
      value,
      false
    );
  }
  add_to_card() {
    //Enviar una lista de deseos
    this.googleAnalyticsService.gtag('event', 'view_item_list', {
      items: [
        {
          item_id: 'SKU_12345',
          item_name: 'jeggings',
          coupon: 'SUMMER_FUN',
          discount: 2.22,
          index: 5,
          item_list_name: 'Related Products',
          item_list_id: 'related_products',
          affiliation: 'Google Store',
          item_brand: 'Gucci',
          item_category: 'pants',
          item_variant: 'black',
          price: 9.99,
          currency: 'USD',
          quantity: 1,
        },
      ],
      item_list_name: 'Related products',
      item_list_id: 'related_products',
    });
    //Enviar información del producto a comprar
    this.googleAnalyticsService.gtag('event', 'add_payment_info', {
      currency: 'USD',
      value: 7.77,
      coupon: 'SUMMER_FUN',
      payment_type: 'Credit Card',
      items: [
        {
          item_id: 'SKU_12345',
          item_name: 'Stan and Friends Tee',
          affiliation: 'Google Merchandise Store',
          coupon: 'SUMMER_FUN',
          currency: 'USD',
          discount: 2.22,
          index: 0,
          item_brand: 'Google',
          item_category: 'Apparel',
          item_category2: 'Adult',
          item_category3: 'Shirts',
          item_category4: 'Crew',
          item_category5: 'Short sleeve',
          item_list_id: 'related_products',
          item_list_name: 'Related Products',
          item_variant: 'green',
          location_id: 'L_12345',
          price: 9.99,
          quantity: 1,
        },
      ],
    });

    //Enviar información del producto que se está viendo
    this.googleAnalyticsService.gtag('event', 'add_shipping_info', {
      currency: 'USD',
      value: 7.77,
      coupon: 'SUMMER_FUN',
      shipping_tier: 'Ground',
      items: [
        {
          item_id: 'SKU_12345',
          item_name: 'Stan and Friends Tee',
          affiliation: 'Google Merchandise Store',
          coupon: 'SUMMER_FUN',
          currency: 'USD',
          discount: 2.22,
          index: 0,
          item_brand: 'Google',
          item_category: 'Apparel',
          item_category2: 'Adult',
          item_category3: 'Shirts',
          item_category4: 'Crew',
          item_category5: 'Short sleeve',
          item_list_id: 'related_products',
          item_list_name: 'Related Products',
          item_variant: 'green',
          location_id: 'L_12345',
          price: 9.99,
          quantity: 1,
        },
      ],
    });

    //agregar a tarjeta
    this.googleAnalyticsService.gtag('event', 'add_to_cart', {
      currency: 'USD',
      value: 7.77,
      items: [
        {
          item_id: 'SKU_12345',
          item_name: 'Stan and Friends Tee',
          affiliation: 'Google Merchandise Store',
          coupon: 'SUMMER_FUN',
          currency: 'USD',
          discount: 2.22,
          index: 0,
          item_brand: 'Google',
          item_category: 'Apparel',
          item_category2: 'Adult',
          item_category3: 'Shirts',
          item_category4: 'Crew',
          item_category5: 'Short sleeve',
          item_list_id: 'related_products',
          item_list_name: 'Related Products',
          item_variant: 'green',
          location_id: 'L_12345',
          price: 9.99,
          quantity: 1,
        },
      ],
    });

    //Lista de deseos
    this.googleAnalyticsService.gtag('event', 'add_to_wishlist', {
      currency: 'USD',
      value: 7.77,
      items: [
        {
          item_id: 'SKU_12345',
          item_name: 'Stan and Friends Tee',
          affiliation: 'Google Merchandise Store',
          coupon: 'SUMMER_FUN',
          currency: 'USD',
          discount: 2.22,
          index: 0,
          item_brand: 'Google',
          item_category: 'Apparel',
          item_category2: 'Adult',
          item_category3: 'Shirts',
          item_category4: 'Crew',
          item_category5: 'Short sleeve',
          item_list_id: 'related_products',
          item_list_name: 'Related Products',
          item_variant: 'green',
          location_id: 'L_12345',
          price: 9.99,
          quantity: 1,
        },
      ],
    });
  }
}
