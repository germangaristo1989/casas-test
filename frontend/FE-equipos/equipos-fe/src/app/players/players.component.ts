import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Jugador, JugadorService } from '@casas/rest';
import { TranslateService } from '@ngx-translate/core';
import { GoogleAnalyticsService } from 'ngx-google-analytics';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss'],
})
export class PlayersComponent implements OnInit, AfterViewInit {
  playerForm = this.fb.group({
    name: null,
    position: [null, Validators.required],
    esMito: [null, Validators.required],
    esTitular: [null, Validators.required],
  });
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  displayedColumns = ['name', 'position'];
  public listaJugadores: Jugador[] = [];

  constructor(
    private fb: FormBuilder,
    private svcJugadores: JugadorService,
    public _MatPaginatorIntl: MatPaginatorIntl,
    private readonly translate: TranslateService,
    private googleAnalyticsService: GoogleAnalyticsService
  ) {
      // asi se registra el path con el nombre de la página en el evento page_view
    this.googleAnalyticsService.pageView('/jugadores', 'Jugadores');
    this.svcJugadores.jugadorList().subscribe((value) => {
      this.listaJugadores = value;
      this.dataSource.paginator = this.paginator;
    });

    const paginatorIntl = new MatPaginatorIntl();

    paginatorIntl.itemsPerPageLabel = this.translate.instant('items');
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit(): void {
    this.svcJugadores.jugadorList().subscribe({
      next: (value) => {
        this.dataSource.data = value;
        this.dataSource.paginator = this.paginator;
      },
      error: () => {
        // capturar el evento cuando el subscribe sale mal
        this.googleAnalyticsService.exception('Esta es una excepción', true);
      },
    });
  }

  onSubmit(): void {
    console.log(this.playerForm.value);
    this.svcJugadores
      .jugadorCreate({
        posicion: this.playerForm.value.position,
        esMito: true,
        esTitular: true,
        nombre: this.playerForm.value.name,
      })
      .subscribe({
        next: (response) => {
          this.ngOnInit();
          this.ngAfterViewInit();
          // evento que se le envia el jugador agregado
          this.googleAnalyticsService.event(
            'Jugador_Agregado',
            'Categoria',
            response.nombre,
            undefined,
            false
          );
        },
        error: () => {
          //enviar la excepción con el error capturado , te informa donde se ha provocado el error
          this.googleAnalyticsService.exception('Error Capturado', true);
        },
      });
  }
}
